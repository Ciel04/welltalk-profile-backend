package com.welltalk.caps.Service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.welltalk.caps.Entity.UserEntity;
import com.welltalk.caps.Repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	//C - Create or Insert a solution record
		public UserEntity insertUser(UserEntity user) {
			return userRepository.save(user);
		}
		
		//R - Read all record from the table named tbl_solution
		public List<UserEntity> getAllUsers(){
			return userRepository.findAll();
		}
		
		//U - Update a solution record
		public UserEntity putUser(String idNo, UserEntity newUserDetails) throws Exception {
			UserEntity user = new UserEntity();
			
			try {
				//steps in updating
				//Step 1 - search the id number of the solution
				user = userRepository.findById(idNo).get();
				
				//Step 2 - update the record
				user.setFirst_Name(newUserDetails.getFirst_Name());
				user.setLast_Name(newUserDetails.getLast_Name());
				user.setEmail(newUserDetails.getEmail());
				user.setCourse(newUserDetails.getCourse());
				user.setContact_No(newUserDetails.getContact_No());
				user.setPassword(newUserDetails.getPassword());
				//Step 3 - save the information and return the value
				return userRepository.save(user);
			}catch(NoSuchElementException nex) {
				throw new Exception("ID Number: " + idNo + " does not exist!");
			}
		}
		//D - Delete a solution record
		public String deleteUser(String idNo) {
			String msg;
			if(userRepository.findById(idNo) != null) { //step 1 - find the reocrd
				userRepository.deleteById(idNo);		//step 2 - delete the record
				
				msg = "ID Number: " + idNo + " is successfully deleted!";
			}else {
				msg = "ID Number: " + idNo + " is NOT found!";
			}
			return msg;
		}
}