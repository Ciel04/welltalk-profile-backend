package com.welltalk.caps.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.welltalk.caps.Repository.UserRepository;

import java.util.List;
import java.util.Optional;

import com.welltalk.caps.Entity.UserEntity;

@RestController
@CrossOrigin(origins = "http://localhost:19006/")
public class UserController {

    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @PostMapping("/user")
    public ResponseEntity<String> createUser(@RequestBody UserEntity user) {
        try {
            userRepository.save(user);
            return ResponseEntity.ok("User has been created.!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error in creating a user: " + e.getMessage());
        }
    }

    @GetMapping("/getAllUser")
    public List<UserEntity> getAllUser() {
        return userRepository.findAll();
    }

    @GetMapping("/userGet/{idNo}")
    public ResponseEntity<UserEntity> getUserById(@PathVariable("idNo") String idNo) {
        Optional<UserEntity> user = userRepository.findById(idNo);
        return user.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
    
    @PutMapping("/user/{idNo}")
    public ResponseEntity<String> updateUser(@PathVariable("idNo") String idNo, @RequestBody UserEntity updatedEntry) {
        Optional<UserEntity> user = userRepository.findById(idNo);
        if (user.isPresent()) {
            UserEntity existingEntry = user.get();
            existingEntry.setFirst_Name(updatedEntry.getFirst_Name());
            existingEntry.setLast_Name(updatedEntry.getLast_Name());
            existingEntry.setEmail(updatedEntry.getEmail());
            existingEntry.setCourse(updatedEntry.getCourse());
            existingEntry.setContact_No(updatedEntry.getContact_No());
            existingEntry.setPassword(updatedEntry.getPassword());
            userRepository.save(existingEntry);
            return ResponseEntity.ok("User's profile has been updated successfully!");
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/user/{idNo}")
    public ResponseEntity<String> deleteUser(@PathVariable("idNo") String idNo) {
        Optional<UserEntity> user = userRepository.findById(idNo);
        if (user.isPresent()) {
        	userRepository.deleteById(idNo);
            return ResponseEntity.ok("User has deleted successfully!");
        } else {
            return ResponseEntity.notFound().build();
        }
    }
   
    
}
