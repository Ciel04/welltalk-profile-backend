package com.welltalk.caps.Entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="tbl_users")
public class UserEntity {
	
	@Id @Column(length = 50)
	private String id_No;
	
	private String first_Name;
	private String last_Name;
	private String email;
	private String course;
	private String contact_No;
	private String password;
	
	public UserEntity(String id_No, String first_Name, String last_Name, String email, String course, String contact_No, String password) {
		super();
		this.id_No = id_No;
		this.first_Name = first_Name;
		this.last_Name = last_Name;
		this.email = email;
		this.course = course;
		this.contact_No = contact_No;
		this.password = password;
	}

	public UserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserEntity(String id_No) {
	    this.id_No = id_No;
	}
	
	public String getId_No() {
		return id_No;
	}

	public void setId_No(String id_No) {
		this.id_No = id_No;
	}

	public String getFirst_Name() {
		return first_Name;
	}

	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}

	public String getLast_Name() {
		return last_Name;
	}

	public void setLast_Name(String last_Name) {
		this.last_Name = last_Name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getContact_No() {
		return contact_No;
	}

	public void setContact_No(String contact_No) {
		this.contact_No = contact_No;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
}
